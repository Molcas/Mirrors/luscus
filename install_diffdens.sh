#!/bin/bash

LUSCUS_CONFIG_DIR=""
# In the case that this script can't find luscus config dir, please set the path manualy
# example LUSCUS_CONFIG_DIR="/home/user/bin/luscus"

# 0. define config dir

if [ ${#LUSCUS_CONFIG_DIR} -lt 1 ]; then
  if [ ! -z $LUSCUS_DIR ]; then
    LUSCUS_CONFIG_DIR=$LUSCUS_DIR
  elif [ -d /etc/luscus ]; then
    LUSCUS_CONFIG_DIR="/etc/luscus"
  elif [ -d $HOME/.luscus ]; then
    LUSCUS_CONFIG_DIR="$HOME/.luscus"
  else
    echo Could not locate the location of the luscus config directory. Most probably it is the directory with luscus executable. Please locate the directory, set the LUSCUS_CONFIG_DIR variable at line 3 of this script to the path of the directory and run this script again.
    stop
  fi
fi

if [ ! -w $LUSCUS_CONFIG_DIR ]; then
  echo You have no writing privileges to the luscus config directory \($LUSCUS_CONFIG_DIR\). Please run this script as superuser or ask your system administrator for help. 
  stop
fi

# 1. make diffdens

if [ ! -e plugins/diffdens/diffdens.exe ]; then
  cd plugins/diffdens
  make
  cp -f diffdens.exe $LUSCUS_CONFIG_DIR
  make cleanall
  cd ..
fi

# 2. write plugin definitions to the calculation.rc

echo "  plugin=diffdens.exe path=$LUSCUS_CONFIG_DIR  description=\"density difference\" type=orbital  arg1=-d1 arg_des1=\"density grid1\"  arg_val1=\" \" arg2=-d2 arg_des2=\"density grid2\"  arg_val2=\" \"  arg3=-a arg_val3=-1.0 arg_des3=weight" >> $LUSCUS_CONFIG_DIR/calculation.rc

echo "  plugin=diffdens.exe path=$LUSCUS_CONFIG_DIR  description=\"density sum\" type=orbital  arg1=-d1 arg_des1=\"density grid1\"  arg_val1=\" \" arg2=-d2 arg_des2=\"density grid2\"  arg_val2=\" \"  arg3=-a arg_val3=+1.0 arg_des3=weight" >> $LUSCUS_CONFIG_DIR/calculation.rc

